# **Focus Management**

## **Deep Work**

### **What is Deep Work?**

Deep work is the term coined by Cal Newport, it means to focus without distraction, on a cognitively demanding task. In layman's terms, it is hard-focus on something. Coding is a perfect example to elaborate deep work. In coding, we need to solve a problem with all the required tools, but to solve the problem we need undivided focus on the required result and process by which we will get there in terms of instruction.
This undivided focus is necessary for all knowledge-related fields.

## **Summary of Deep Work Book**

### **Paraphrase all the ideas in the above videos and this one in detail.**

- In the first video tells about favorable time to deep work is more than 45 minutes until you have a focus on work or complete the task.

- In the second video tells us about deadlines are good because it gives us motivational signals.

- It also helps to focus on work without any breaks or distractions during working time.

- In third video tells a summary of the deep workbook. We should practice deep work in day-to-day life.

- Tells about the strategies we can implement for deep work:

    - We can minimize distractions by scheduling breaks.
    - Early morning is the best time to deep work because that time distractions are minimum.
    - Evening shut down rituals and create a plan for the next day.
  
### **How can you implement the principles in your day to day life?**

Enumerated are a few ways to implement deep work in my daily life:

- **Schedule Distractions:** Giving into any distraction is training your brain to give in to any and all distractions. Placing boundaries is crucial to break this habbit of giving into distractions. This can be accomplished by writing down the next distraction break I'm going to have and then focusing on work until the next distraction.

- **Rythmic Deep Work Schedule:** A simple regular habbit of sitting to work at the same time, removes the motivational barrier that is otherwise required to start the work. You can also switch rythmically between deep and shallow work. Scheduling deep work in ad-hoc manner also doesn't work. Focusing on one thing at the same time everyday is much more effective. So it better to consistently complete 4 hours of Deep Work per day at the same times each day.

- **Evening Shutdown:** I can incorporate an evening shut-down routine so as to avoid. This can be something like writing down all the unfinished tasks and what can be done to achieve the task next day. This allows our brain to not think about future stuff that need to get done, subconciously.

## **Dangers of Social Media**

### **Your key takeaways from the video**

- Social Media is not a fundamental technology.

- It costs the productivity of people badly.

- Social media are designed to be addictive

- If we spend a large amount of the day in a state of fragmented attention then our concentration power decreases.