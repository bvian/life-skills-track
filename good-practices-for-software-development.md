# **Good Practices for Software Development**

## **Review Questions**

### **Question 1**

**What is your one major takeaway from each one of the 6 sections. So 6 points in total.**

- Make notes while discussing requirements with your team.

- Requirements have changed? - If the current deadline will not be met due to the changes, inform relevant team members about the new deadline.

- Explain the problem clearly, mention the solutions you tried out to fix the problem.

- Join the meetings 5-10 mins early to get some time with your team members.

- Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.

- Work when you work, play when you play is a good thumb rule.

### **Question 2**

**Which area do you think you need to improve on? What are your ideas to make progress in that area?**

- Have an eye on the industry, and keep learning from seniors.

- Need to contribute to open source and practice regularly and consistently to get familiar with best practices and industry standards.

- Always ask for feedback, and handle criticism.