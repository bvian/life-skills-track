# **Learning Process**

## **1. How to Learn Faster with the Feynman Technique?**

- ### **Feynman Technique**
    
    The Feynman Technique is a learning method named after Richard Feynman. The main concept behind Feynman Technique is to try to explain it simply. Like in general life by attempting to explain a concept in simple terms, Anyone can quickly see where you have a good understanding of that concept. You’ll also be able to instantly pinpoint your problem areas because they’ll be the areas where you either get stuck or where you end up resorting to using complex language and terminology. The ability to relay complex ideas to others in simple, intuitive ways – the Feynman Technique is a method for learning or reviewing a concept quickly by explaining it in plain, simple language.

- ### **Different ways to implement this technique in your learning process**
  
    <img src="https://blog.doist.com/wp-content/uploads/2020/02/feynman-technique_graphic-1_resize-1-1000x618.png" width=40%  height=40%>

    Feynman Technique is based on simple explaining using simple language and easily understandable examples, steps involved are as follows:-
    
    - **Step 1**: Grab a sheet of paper and write the name of the concept at the top. You can use pretty much any concept or idea – even though the technique is named after Feynman, it’s not limited solely to math and science.
    
    - **Step 2**: Explain the concept in your own words as if you were teaching it to someone else. Focus on using plain, simple language. Don’t limit your explanation to a simple definition or a broad overview; challenge yourself to work through an example or two as well to ensure you can put the concept into action.
    
    - **Step 3**: Review your explanation and identify the areas where you didn’t know something or where you feel your explanation is shaky. Once you’ve pinpointed them, go back to the source material, your notes, or any examples you can find to shore up your understanding.
    
    - **Step 4**: If there are any areas in your explanation where you’ve used lots of technical terms or complex language, challenge yourself to re-write these sections in simpler terms. Make sure your explanation could be understood by someone without the knowledge base you believe you already have.

## **2. Learning How to Learn TED talk by Barbara Oakley**

- ### **Learning TED talk**

    <img src="https://miro.medium.com/v2/resize:fit:720/format:webp/1*zooFZk6dvZFHV757DlBykQ.png" width=40% height=40%>

    Here is the list of topics that Barbara Oakley talked about in her video:-

    - **In Focused mode**, consider an example of a pinball machine, there your thoughts bounce and collide with the historical events, your previous learning, and concepts you already know.
  
    - I**In Diffuse mode**, when the concept is totally new to you, it feels like rubber balls are very far, when we think of thought it ranges very widely as the balls are far apart, which leads to distraction as you u are unable to do careful thinking.

    To deal with this problem an example of relaxing was explained. when relaxing and thinking over the thought bring your focus from diffuse mode to focused mode.

    **Procrastination**, you look at the problem you rather not do you feel pain in your brain, the first way is to go working through it after a while pain goes away, and the second way is to turn your attention away to feel better, this practice may harm you in a long way, to reduce it we have Pomodoro technique.

    **The Pomodoro Technique** - get a timer of 25min without any distractions and do your work with full attention for those 25 mins. When u did do something fun to relax. By doing so you are simultaneously you are practicing your attention and relaxing.

-  ### **Some of the steps that can take to improve the learning process**

    - Focused and diffuse modes (when stuck, relax, let your mind flow in diffuse mode, then think in focus). Learning something difficult takes time to give yourself some time to go back and forth in these states
    - Using the Pomodoro technique to avoid procrastination helps a lot, very useful for focusing (been using it)
    - Use Metaphors, they provide powerful techniques for learning
    - Practice makes permanent- neurons become linked together through repeated use. Test yourself and indulge deeply in your Homework/drill.
    - Spaced repetition- repeat what you’re trying to retain over a number of days, the chance to remember will be higher
    - Exercise & Sleep properly:
    - increase our ability to both learn & remember
    - cleans toxic products in your brain
    - erases the less important parts of memories
    - strengthens areas that you need or want to remember
    - A Recall is the best learning technique (instead of passive readings)
    - Minimize highlights- try to understand the main idea first and then highlight
    - Daily “to do” list / daily journal works wonder

 ## **3. Learn Anything in 20 hours**

- ### **Key takeaways:-**

    Everyone should always find free time in his schedule to learn skills. It has been suggested in the 10,000 rule that in order to become an expert in a particular field, one must practice the relevant skills for a total of 10,000 hours. It might be true for highly competitive fields which require very high performance but to learn new skills, the first 20 hours are crucial.

    In order to learn any skill in 20 hours, Josh Kaufman offers a four-step process with advice from his own experience

    - Deconstruct the skill
    - Learn enough to self-correct
    - Remove practice barriers
    - Practice at least 20 hours

- ### **Some of the steps that you can while approaching a new topic**
    
    - **Deconstruct the skill** by analyzing the skill and breaking it down into smaller parts, it is easier to identify and learn the building blocks of the desired skill. For example, if I wanted to learn how to program a robot, I might want to learn about electricity, a specific computer programming language, and the basics of physics. By learning this smaller set of skills, I would be more prepared to achieve my desired complex skill of programming a robot.

    - **Learn enough to self-correct** in the event of learning a new skill, independently, it is important to learn just enough to be able to self-correct. Being able to identify errors and fix mistakes is a critical assessment tool when trying to learn something new.

    - **Remove practice barriers** another key part of this process is to remove practice barriers that might interfere with learning. In this case, a barrier could be considered anything that would be a distraction to practicing the skill. Some barriers could include watching television, playing on cell phones, listening to music, or just hanging out with friends. Therefore, it is important to set aside undivided time that is dedicated to practicing the skill.

    - **Practice at least 20 hours** The last piece of advice that Josh Kaufman offers is to simply practice at least 20 hours. Based on his research, he has found that 20 hours is the magic number to become proficient in a skill.