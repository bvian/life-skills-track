# **OOPS**

Originated in 1970, Object oriented programming (OOPs) is one of the fundamental concept which is used by almost all the developers at some point in their career. Just like C++, C#, Python, Java is another programming language which uses the concept of object oriented programming. 

## **Introduction**

Object means a real word entity such as pen, chair, table etc. Object-Oriented Programming is a methodology or paradigm to design a program using classes and objects. It simplifies the software development and maintenance by providing some concepts:

![OOPs ](https://static.javatpoint.com/images/java-oops.png)

- ### **Objects:** 
  Instances of a class created with specifically defined data. Objects can correspond to real-world objects or an abstract entity. When class is defined initially, the description is the only object that is defined.

- ### **Class:**
 
    <img src="https://lh3.googleusercontent.com/CQU33EjdjbJamtanRJUpecpT9Au6VhIQKaMKoEa3JCBzqV-DMabnuQyY6Tb0AQBn_qgizK5Ew4ROmti5T7ZpVe8LSS_lwDrZxuYqMTx3jSnOaJ5gcaphtxjnat7GnZKs1fIyP2pm" width=50% height=50%>

  Class is one of the Basic concepts of OOPs which is a group of similar entities. It is only a logical component and not the physical entity. Lets understand this one of the OOPs Concepts with example, architectural diagram of a building (blueprint) taken as a class and object as a Building.

  
- ### **Inheritance:**
  
  <img src="https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2017/04/Inheritance-1-468x300.png" width=30%  height=30%>

  Inheritance is the object-oriented programming concept where an object is based on another object. Inheritance is the mechanism of code reuse. The object that is getting inherited is called the superclass and the object that inherits the superclass is called a subclass. We use public keyword in c++ to implement inheritance. Below is a simple example of inheritance in c++.
    ```
        #include <iostream>

        class superClassA {
        public:
            void superPrint() {
                std::cout << "SuperClassA\n";
            }
        };

        class subClassB: public superClassA {
        public:
            void subPrint() {
                std::cout << "SubClassB\n";
            }
        };

        int main() {
            subClassB a;
            a.superPrint();
            a.subPrint();
            return 0;
        }
    ```
    **Output**

    ```
      SuperClassA
      SubClassB
    ```
    
    **Inheritance is further classified into 4 types:**

    <img src="https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2017/04/Inheritance-types-java.png"  width=45% height=45%>

- ### **Polymorphism:**
  
  <img src="https://lh6.googleusercontent.com/VCeBrsJDrh_Dyg5nHEF8Nwj4VHVBrCVZkS6elTy0O2qMcKiCMCzjrUEDZ742P8GH6QkEtr7NgphcgYbGuw9s9ujyWqHOGklrfbCgWyYp1Lov49MLZYCko5ovrtvGMlCicHpq05Ba" width=30% height=30% >

  Polymorphism refers to one of the OOPs concepts in c++ which is the ability of a variable, object or function to take on multiple forms. For instance, a woman can be seen as a lawyer, a mother, a sister, and a daughter.

  ```
    #include <iostream>

    class calc {
    public:
        void func(int x) {
		    std::cout << "value of x is " << x << endl;
	    }

	    void func(double x) {
		    std::cout << "value of x is " << x << endl;
	    }

	    void func(int x, int y) {
		    std::cout << "value of x and y is " << x << ", " << y << endl;
	    }
    };

    int main() {
	    calc obj1;

	    obj1.func(7);
	    obj1.func(9.132);
	    obj1.func(85, 64);
	
      return 0;
    }


  ```
    **Output**
    ```
      value of x is 7
      value of x is 9.132
      value of x and y is 85, 64
    ```
     In the above example, a single function named function func() acts differently in three different situations, which is a property of polymorphism.


- ### **Abstraction**
  Abstraction is one of the OOP Concepts in c++ which is an act of representing essential features without including background details. It is a technique of creating a new data type that is suited for a specific application.
  
  <img src="https://lh3.googleusercontent.com/F1-KtD-fAyo6_Qvz7xJJe8FIwI6opylVqlwh_jIWTrpt1nJGfICZBbBuwjdn3iewTL-Ap-qDCv57r6MLIuvu1--7m_03Te2M5WmVWPrztvk6hh-yerg13b8M6p_wVLbo1NCJxDZ3" width=55% height=55%>

  For example, While using a washing machine all you do is press buttons, you don’t have to deal with device’s internal wiring. Here the internal wiring details are abstracted for normal users.
  
  ```
    // C++ Program to Demonstrate the
    // working of Abstraction
    #include <iostream>

    class implementAbstraction {
    private:
	    int a, b;

    public:
	    // method to set values of
	    // private members
	    void set(int x, int y) {
		    a = x;
		    b = y;
	    }

	    void display() {
		    std::cout << "b = " << b << endl;
		    std::cout << "a = " << a << endl;
	    }
    };

    int main() {
	    implementAbstraction obj;
	    obj.set(10, 20);
	    obj.display();
	    return 0;
    }
  ```
  **Output**
  ```
    a = 10
    b = 20
  ```

  You can see in the above program we are not allowed to access the variables a and b directly, however, one can call the function set() to set the values in a and b and the function display() to display the values of a and b. 

- ### **Encapsulation**
  Encapsulation is the technique used to implement abstraction in object-oriented programming. Encapsulation is used for access restriction to class members and methods. Access modifier keywords are used for encapsulation in object oriented programming. For example, encapsulation in java is achieved using private, protected and public keywords.

  <img src="https://lh6.googleusercontent.com/wPqHUcA465TF5ozHBzksywzAvRGNgQtR8I5T-8JnrKI0BZCf6dswWtAB_VVQuI5wJ5OspUf7_k6ag-ZM19Cj5OhA9Ha_xjNtxingndE3S_Xg9XyHgH-SexknxA0NCuFd2Ly6z_L9" width=30% height=30%>

  For example, Herbivorous animals are a class. All the animals that eat plants to survive come under this category. Hence, all the animals, such as cows, goats, deer, etc., encapsulate under a category of herbivorous animals. Moreover, the category distribution protects against other animal categories, such as carnivorous animals.

  ```
    // C++ program to demonstrate Encapsulation
    #include <iostream>

    class Encapsulation {
    private:
      // Data hidden from outside world
      int x;
      
    public:
      // Function to set value of variable x
      void set(int a) {
        x = a;
      }
      // Function to return value of variable x
      int get() {
        return x;
      }
    };

    int main() {
	    Encapsulation obj;
	    obj.set(5);
	    srd::cout << obj.get();
	    return 0;
    }
  ```

  **Output**
  ```
    5
  ```

  In the above program, the variable x is made private. This variable can be accessed and manipulated only using the functions get() and set() which are present inside the class. Thus we can say that here, the variable x and the functions get() and set() are bound together that is encapsulation.

## **Advantages of OOPs (Object-Oriented Programming System)**

  - **Modularity.** Encapsulation enables objects to be self-contained, making troubleshooting and collaborative development easier.

  - **Reusability.** Code can be reused through inheritance, meaning a team does not have to write the same code multiple times.

  - **Productivity.** Programmers can construct new programs quicker through the use of multiple libraries and reusable code.

  - **Easily upgradable and scalable.** Programmers can implement system functionalities independently.

  - **Interface descriptions.** Descriptions of external systems are simple, due to message passing techniques that are used for objects communication.

  - **Security.** Using encapsulation and abstraction, complex code is hidden, software maintenance is easier and internet protocols are protected.

  - **Flexibility.** Polymorphism enables a single function to adapt to the class it is placed in. Different objects can also pass through the same interface.

## **Disadvantages of OOPs (Object-Oriented Programming System)**

  - **Functional programming.** This includes languages such as Erlang and Scala, which are used for telecommunications and fault tolerant systems.

  - **Structured or modular programming.** This includes languages such as PHP and C#.
  - **Imperative programming.** This alternative to OOP focuses on function rather than models and includes C++ and Java.

  - **Declarative programming.** This programming method involves statements on what the task or desired outcome is but not how to achieve it. Languages include Prolog and Lisp.

  - **Logical programming.** This method, which is based mostly in formal logic and uses languages such as Prolog, contains a set of sentences that express facts or rules about a problem domain. It focuses on tasks that can benefit from rule-based logical queries.

## References
  - https://www.researchgate.net/publication/221322123_The_OOPS_framework_high_level_ions_for_the_development_of_parallel_scientific_applications

  
  - https://www.javatpoint.com/java-oops-concepts
  
  - https://www.naukri.com/learning/articles/oops-concepts-in-java/

  - https://www.geeksforgeeks.org/abstraction-in-cpp/
  
  - https://www.geeksforgeeks.org/encapsulation-in-cpp/
  
  - https://www.geeksforgeeks.org/object-oriented-programming-in-cpp/