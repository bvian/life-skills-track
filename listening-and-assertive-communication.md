# **Listening and Active Communication**

## **1. Active Listening**

- Avoid getting distracted by your own thoughts. Focus on the speaker and topic instead.
- Try not to interrupt the other person. Let them finish and then respond.
- Use door openers. These are phrases that show your are interested and keep the other person talking.
- Show that you are listening with body language.
- If appropriate take notes during important conversations.
- Paraphrase what other have said to make sure you are on the same page.
  
  <img src="https://cdn.harappa.education/wp-content/uploads/2021/07/18045021/Active-Listening.jpg" width=20%  height=20%>

## **2. Reflective Listening**

According to Fisher's model, the key points of Reflective Listening are.
- The listener's focus should be on the other's point of view.
- Listeners should respond to things the other says about them.
- Listeners should accept and clarify what the other has said at that person's level.
- Listeners should express their feelings for others.
- Listeners could respond with Ambivalent feelings and Negative feelings
  
  <img src="https://i0.wp.com/chieforganizer.org/wp-content/uploads/2015/02/5a686f9adbe676c5e3f6cc43b29f5579.gif" width=25%  height=25%>

## **3. Reflection**

### **Obstacles in Listening**

Listening requires us to concentrate and ignore all the other environmental and physiological distractions. One may hear the words another person utters without really understanding them as a result of obstacle to listening.

Several distractions that interfere with our ability to listen effectively can be physical, physiological or psychological obstacles.

  <img src="https://www.marketing91.com/wp-content/uploads/2019/07/Barriers-to-Effective-Listening-3.jpg" width=30%  height=30%>

  - **Barriers from the environment**
    
    - **Physical barriers**, which emanate from the environment, include anything that can interfere with your ability to “hear” the message or any distractions that would interfere with your ability to focus. **Physical barriers** include:
      - excessive noise levels,
      - uncomfortable environmental conditions, and
      - excessive visual stimulation.
  
  - **People-related Barriers**
  
    - **Physiological barriers** include physical and mental distractions because listening is both a physical and mental activity.

      - **Physical distractions** arise when the listener suffers from ill health, fatigue, sleeplessness or hearing problems.
      - **Mental distraction** emanates as a result of our own mind wandering 100 miles an hour; thereby hindering us to focus on anything else.
  
    - **Psychological barriers** cover the value system and the behavioural aspects. It may also be on account of hierarchical differences.
  
        Some examples where listening fails to be effective on account of people-related factors are as follows:
        - The speaker speaks in a shrill voice that cannot reach the listener.
        - The speaker speaks very rapidly or with an accent that is not clear.
        - The listener of the message does not consider the speaker to be well informed.
        - The listener lets the mind wander rather than stay focused on the message.
        - The listener perceives the speaker to be lacking in depth or not having adequate authority.

### **Improving Listening Skills**

The following keys which help us become an active and effective listener:
- **Stop talking:** The first step to becoming a good listener is to stop talking. The speaker cannot speak and get the message across if the listener continues to talk.

- **Control your surroundings:** Whenever possible, remove competing sounds. Close windows or doors, turn off electronic gadgets, and move away from loud people, noisy appliances, or engines.

- **Keep an open mind:** Most of us sift through and filter information based on our own biases and values. Empathize with the speaker for having put up great work to come up with the message.

- **Capitalize on lag time:** Make use of the quickness of your mind by reviewing the speaker’s points. Try to guess and anticipate what the speaker’s next point will be.

- **Listen between the lines:** Focus both on what is spoken and what is unspoken. Apart from the bare verbal message, listen actively for the emotional content as well. Be alert to all cues and carefully observes the nonverbal behaviour of the speaker to get the total picture.

- **Focus on ideas, not appearance:** Pay attention to the content of the message, not on its delivery. Avoid being distracted by the speaker’s looks, voice, or mannerisms.

- **Hold your fire:** Discipline yourself to listen to the speaker’s entire message before reacting. Such restraint may enable you to understand the speaker’s reasons and logic before you jump to hasty conclusions.

- **Take selective notes:** In some situations thoughtful note-taking may be required to record important facts that must be recalled later.

- **Provide feedback:** Let the speaker know that you are listening. Maintain eye contact, nod your head and ask relevant questions at appropriate times.

## **4. Types of Communication**

### I switch to a passive communication style in my daily life. examples -
- often feel anxious because life seems out of my control
- often feel depressed because I feel stuck and hopeless
- often feel confused because I ignore my own feelings
- am unable to mature because real issues are never addressed

### I switch to a Aggressive communication style in my daily life. Sometimes -
- I become very impulsive.
- I have low frustration tolerance
- I act threateningly and rudely.
- I does not listen well
- I interrupt frequently

### I switch to a Passive Aggressive communication style in my daily life. Sometimes - 
- I use facial expressions that don't match how I am feel - i.e., smiling when angry.
- I deny that there is a problem
- I isolate myself from people I don't like around me.
- I appear cooperative but I’m not.

### I can make communication assertive by -
- Direct eye contact shows that the speaker is strong and not intimidated.
- Facial expression. Expressions that are neither angry nor anxious are essential for sending the right message.
- Tone of voice. A strong voice conveys assertiveness, but raising one’s voice shows aggression and is likely to be met with anger.
- Don't be afraid to say no. Something that’s important to know about being assertive: we need to let go of the need to please everyone. As much as we’d all like to be able to do this, it just isn’t possible. 
- Positive language. For example, making a negative request (“Will you stop leaving your papers all over the house?”) is less effective than a positive request (“Here is a divider I’ve set up. Will you please place your papers here?”). 

## References
- https://en.wikipedia.org/wiki/Reflective_listening
- https://www.youtube.com/watch?v=rzsVh8YwZEQ
- https://www.researchgate.net/publication/358816657_Active_Listening
- http://www.analytictech.com/mb119/reflecti.htm
- https://hrdqu.com/communication-skills-training/learning-to-overcome-barriers-to-listening-skills-in-the-workplace/